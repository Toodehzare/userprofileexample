<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');
Route::get('welcome', 'PostController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/Create', 'PostController@create')->name('createpost');
Route::post('store','PostController@store')->name('storepost');
Route::get('/Edit/{post}', 'PostController@edit')->name('editpost');
Route::get('/Delete', 'PostController@delete')->name('deletepost');
Route::post('/Update/{id}', 'PostController@update')->name('updatepost');
Route::get('/Delete/{id}', 'PostController@destroy')->name('destorypost');
Route::get('/Show/{id}', 'PostController@show')->name('showpost');
Route::get('Profile','UserController@profile')->name('profilepost');
Route::post('Profile','UserController@upload_avatar')->name('avatarpost');

Route::post('/comment/store', 'CommentController@store')->name('comment.add');


Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');