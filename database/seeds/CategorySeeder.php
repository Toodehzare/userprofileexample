<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date =  [
            ['category_name' => 'Information Technology'],
            ['category_name' => 'Electronic']
            ];
        Category::insert($date);
    }
}
