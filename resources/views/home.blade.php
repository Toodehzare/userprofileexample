@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Post Lists

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Img</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Published</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <a href="/Create" class="btn btn-primary pull-right">Create New Post</a>
                        @foreach($posts as $post)
                            <tr>
                                <th><img src="/img/posts/{{ $post->post_img }}" width="50px" height="50px" ></th>
                                <th>{{ $post->title }}</th>
                                <th>{{ $post->description }}</th>
                                <th>{{ $post->published_at }}</th>
                                <th><a href="/Edit/{{ $post->id }}" class="btn btn-sm btn-warning">Edit</a> <a href="/Delete/{{ $post->id }}" class="btn btn-sm btn-danger">Delete</a></th>

                            </tr>
                        @endforeach
                    </table>
                        {{ $posts->links() }}
                </div>
                <a href="/" class="btn btn-sm btn-danger">Back</a>
            </div>
        </div>
    </div>
</div>
@endsection
