@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <img src="/img/avatars/{{ $user->avatar }}" style="width: 100px; height: 100px;border-radius: 50%;">
                <h5>{{ $user->name }}'s Profile</h5>
                <h6>Email Address : {{ $user->email }}</h6>
                <h6>Created Date : {{ $user->created_at }}</h6>
                <form enctype="multipart/form-data" action="{{route('profile')}}" method="POST">
                    <label>Update Profile Picture</label>
                    <input type="file" name="avatar">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="submit" class="btn btn-sm btn-primary" value="Upload Avatar" style="margin-top: 10px;">
                </form>
            </div>
            <a href="/home" class="btn btn-sm btn-danger">Back</a>
        </div>
    </div>
@endsection