<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Dashboard</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="col-md-2">
                    <strong>
                        Category
                    </strong>
                    <hr>
                    @foreach($category as $cat)
                        <button type="button" class="btn btn-sm btn-danger" style="margin:2px; padding: 2px;font-size: 12px;">{{$cat->category_name}}</button>
                    @endforeach
                </div>
                    <div class="col-sm-6">


                        @foreach($posts as $post)
                            <div class="row">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="col-sm-2">
                                            <img src="/img/posts/{{ $post->post_img }}" style="display: inline-block;" width="100px" height="100px;">
                                        </div>
                                        <div class="col-sm-4">
                                            <a href="/Show/{{$post->id}}"> {{$post->title}} </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach


                            {{ $posts->links() }}
                    </div>
            </div>
        </div>
    </body>
</html>
