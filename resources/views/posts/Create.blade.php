@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Post</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{route('storepost')}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" required>
                                <label>Description</label>
                                <textarea required class="form-control" name="description"></textarea>
                                <label>Post Image</label>
                                <input type="file" name="post_image">
                                <label>Category</label>
                                <select class="form-control" name="category">
                                    @foreach($cat as $category)
                                        <option name="catopt"  value="{{$category->id}}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                                <input type="submit" class="btn btn-sm btn-primary" name="Publish"  value="Publish" style="margin-top: 10px;">
                                <input type="submit" class="btn btn-sm btn-default" name="Draft" value="Draft" style="margin-top: 10px;">
                            </div>
                        </form>
                            <a href="/home" class="btn btn-sm btn-danger">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
