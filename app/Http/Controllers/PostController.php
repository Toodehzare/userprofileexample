<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::whereNotNull('published_at')->paginate(5);
        $category = Category::all();
        return view('welcome',compact(['posts','category']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cat = Category::all();
        return view('posts.Create',compact('cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $post_request = new Post();
        $post_request->title = $request->title;
        $post_request->description = $request->description;
        $post_request->category_id = $request->category;
        if ($request->has('Publish'))
        {
            $post_request->published_at = date('Y-m-d H:i:s');
        }else
        {
            $post_request->published_at = null;
        }


        if($request->hasFile('post_image')){
            $avatar = $request->file('post_image');
            $filename = time() . "." . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('/img/posts/'.$filename));
            $post_request->post_img =  $filename;
        }

        $post_request->author = Auth::user()->name;
        $post_request->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('id',$id)->first();

        return view('posts.Show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $cat = Category::all();
        return view('posts.Edit',compact(['post','cat']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $post = Post::where('id',$id)->first();
        if ($request->has('Edit'))
        {
            $post->published_at = date('Y-m-d H:i:s');
        }else
        {
            $post->published_at = null;
        }

        $post->title = $request->title;
        $post->description = $request->description;
        $post->category_id = $request->category;
        if($request->hasFile('post_image')){
            $avatar = $request->file('post_image');
            $filename = time() . "." . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('/img/posts/'.$filename));
            $post->post_img =  $filename;
        }
        $post->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::where('id',$id)->first();
        $post->delete();
        return redirect()->back();
    }
}
